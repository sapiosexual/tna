package controllers

/**
  * Created by exfool on 29.11.16.
  */

object Utils {
	class Cloud(val tweets: List[Tweet], val tags:List[String])

	class Tweet(val person:String, val text:String, val date:String, val id:String, val rating:String){
		def nonEmpty:Boolean = !text.isEmpty
	}

	class HitStatistic(val dates: List[String], val hits:List[String]){
		def nonEmpty:Boolean = dates.nonEmpty & hits.nonEmpty
	}

	def isAllDigits(x: String): Boolean = x forall Character.isDigit


	//	def loadDataFrame(path:String): Frame[Int, Int, String] ={
//		val file = CsvFile(path)
//		val df = CsvParser.parse(file) //(List("date", "id", "text", "lang", "retweets", "likes", "timestmp", "username"))
//
//		df
//	}

}
