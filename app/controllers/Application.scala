package controllers


import com.github.marklister.collections.immutable.CollSeq8
import com.github.marklister.collections.io.CsvParser
import engine.classes.entities.cosine.CosineComputer
import engine.classes.entities.document.DocumentImpl

import engine.classes.structures.indeces.{InvertedIndex}
import org.joda.time.DateTime
import play.api.mvc._

import scala.collection.immutable.{ListMap}
import scala.collection.mutable
import scala.collection.mutable.ListBuffer


class Application extends Controller {


	val data: CollSeq8[String, String, String, String, String, String, String, String] = CsvParser[String, String, String, String, String, String, String, String].parseFile("conf/merged.csv")
	val lookup: Map[String, (String, String, String, String, String, String, String, String)] = (data._2 zip data).toMap


	//val content: Iterator[Array[String]] = Source.fromFile("conf/merged.csv").getLines.map(_.split("\","))

	//	df.mapRowIndex( case (ca, id, text, ln, rc, fc, ts, name) => new DocumentImpl(text, id))


	var documents: ListBuffer[DocumentImpl] = mutable.ListBuffer[DocumentImpl]()

	for (raw_row <- lookup.valuesIterator) {
		if (Utils.isAllDigits(raw_row._2)) {
			documents += new DocumentImpl(raw_row._3.replace("#", ""), raw_row._2.toLong)
		}
	}
	var index = new InvertedIndex()
	index.addAll(documents.toList)

	var computer = new CosineComputer(index)
	computer.prepareNorms(documents.toList)

	def index_page = Action {

		Ok(views.html.index("T.N.A."))
	}


	def search(query: String = "") = Action {

		//("created_at", "id_str", "text", "lang", "retweet_count", "favorite_count", "timestamp_ms", "screen_name")
		//val query: String = ""


		val result = computer.processQuery(new DocumentImpl(query, 1))
		//println(result.map(x => x._1.toString + ":" + x._2.toString).mkString(","))
		val fnd = find(result)
		val gb_tags = (
			fnd.map(c => c.tags).toList.flatten.map(_.toString).distinct.toSet[String] --
				query.split(" ").distinct.toSet[String]
			).map(_.toString).toList
		Ok(views.html.search(gb_tags, fnd, getHitStatistic(result), query = query))
	}

	def getTagsGlobal(result: Array[(Long, Double)]): List[String] = {
		var tg = List[String]("I", "hate", "you", "just", "die")

		tg
	}

	def find(results: Array[(Long, Double)]): ListBuffer[Utils.Cloud] = {
		var clouds = ListBuffer[Utils.Cloud]()

		var cntr = 0
		var buf_tweet = mutable.ListBuffer[Utils.Tweet]()
		var buf_tags = mutable.ListBuffer[String]()
		val res_iter = results.iterator
		while (res_iter.hasNext && cntr < 101) {
			var (id, rating) = res_iter.next()

			cntr += 1
			val doc = lookup(id.toString)


			//"2016-07-04 22:23:00"
			val format = new java.text.SimpleDateFormat("yyyy-MM-dd")

			val date = new DateTime(doc._7.toLong).toDateTime.toString("yyyy-MM-dd HH:mm")

			//doc._1.toString
			//new Utils.Tweet("exfool", "Something greatfyll happied !!1", "2016-02-10 12:12", "123142", "9.1"),
			buf_tweet += new Utils.Tweet(doc._8.toString, doc._3.toString, date.toString, doc._2.toString, rating.toString)
			buf_tags ++= findHashtags(doc._3.toString)


			if (cntr % 5 == 0) {
				clouds += new Utils.Cloud(buf_tweet.toList, buf_tags.toList)
				buf_tweet = mutable.ListBuffer[Utils.Tweet]()
				buf_tags = mutable.ListBuffer[String]()
			}

		}
		if (cntr % 5 != 0) {
			clouds += new Utils.Cloud(buf_tweet.toList, buf_tags.toList)
			buf_tweet = mutable.ListBuffer[Utils.Tweet]()
			buf_tags = mutable.ListBuffer[String]()
		}
		//

		// implementation here
		// (val person:String, val text:String, val date:String, val id:String, val rating:String)
		//		clouds += new Utils.Cloud(
		//			List[Utils.Tweet](
		//
		//				new Utils.Tweet("exfool", "Something greatfyll happied now!!1", "2016-02-10 12:12", "123142", "9.1"),
		//				new Utils.Tweet("exfool", "Something greatfyll happied now!!1", "2016-02-10 12:12", "123142", "9.1")
		//			),
		//			List[String]("great", "now", "now", "again", "happy")
		//		)


		clouds
	}

	def findHashtags(str: String): ListBuffer[String] = {
		val result = mutable.ListBuffer[String]()
		val pattern = """#([\w\d]+)""".r
		pattern.findAllIn(str).matchData foreach {
			m => result += m.group(1)
		}
		print(result)
		result
	}

	def getHitStatistic(results: Array[(Long, Double)]): Utils.HitStatistic = {
		//var hs = new Utils.HitStatistic(List[String](), List[String]())

		val dates = mutable.ListBuffer[String]()
		for (res <- results) {
			val doc = lookup(res._1.toString)
			val date = new DateTime(doc._7.toLong).toDateTime.toString("yyyy-MM-dd HH:mm").toString
			dates += date
		}
		val gpd = dates.groupBy(identity).mapValues(_.size)
		val kv = ListMap(gpd.toSeq.sortWith(_._1 < _._1): _*)

		val date_list = kv.keys.toList.map(_.toString + ":00")
		val hit_frq = kv.values.toList.map(_.toString)
		// implementation here
		val hs = new Utils.HitStatistic(date_list, hit_frq)
		//		val hs = new Utils.HitStatistic(
		//			List[String](
		//				"2016-06-04 22:23:00", "2016-07-04 22:23:00", "2016-08-04 22:23:00",
		//				"2016-10-04 22:23:00", "2016-11-04 22:23:00", "2016-12-04 22:23:00"
		//			),
		//			List[String]("14", "32", "59", "42", "14", "74")
		//		)

		hs
	}

}