package engine.traits.structures

import engine.traits.entities.{Document, PostingList}
/**
  * Created by koord on 19.11.2016.
  */
trait Index[+P <:PostingList] {

  def add(doc: Document)
  def addAll(docs: List[Document])

  def remove(doc: Document)
  def removeAll(ids: List[Document])

  def find(term: String) : P

  def intersect[L >: P](postings1: L, postings2: L) : P
  def union[L >: P](postings1: L, postings2: L) : P
  def negation[L >: P](postings: L) : P

  def idf(term: String) : Double
  def tfidfs(term: String) : scala.collection.mutable.Map[Long, Double]



}
