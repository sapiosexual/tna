package engine.traits.structures

/**
  * Created by koord on 19.11.2016.
  */
trait WildcardIndex {

  def add(term: String)
  def addAll(terms: String)

  def remove(term: String)
  def removeAll(term: String)

  def lookup(term: String) : List[String]

}
