package engine.traits.entities

import scala.collection.mutable

/**
  * Created by koord on 19.11.2016.
  */
trait PostingList {

  def add(docId: Long)
  def addAll(docIds: List[Long])

  def remove(docId :Long)
  def removeAll(docIds: List[Long])

  def postings: mutable.Map[Long, Long]
}
