package engine.traits.entities

/**
  * Created by koord on 19.11.2016.
  */
trait Document {
  def text : String
  def text_=(value:String)
  def id : Long
  def id_=(value:Long)

}
