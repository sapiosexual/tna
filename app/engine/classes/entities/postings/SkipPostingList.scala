package engine.classes.entities.postings

import engine.traits.entities.PostingList

import scala.collection.mutable

/**
  * Created by koord on 19.11.2016.
  */
class SkipPostingList extends PostingList {
  override def add(docId: Long): Unit = ???

  override def addAll(docIds: List[Long]): Unit = ???

  override def remove(docId: Long): Unit = ???

  override def removeAll(docIds: List[Long]): Unit = ???

  override def postings: mutable.Map[Long, Long] = ???
}
