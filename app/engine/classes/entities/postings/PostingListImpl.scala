package engine.classes.entities.postings

import engine.traits.entities.PostingList

import scala.collection.mutable

/**
  * Created by koord on 19.11.2016.
  */
class PostingListImpl extends PostingList {

  private val _map = mutable.Map[Long, Long]()

  override def add(docId: Long): Unit = if (_map.contains(docId)) _map(docId) = _map(docId) + 1 else _map.put(docId, 1.toLong)

  override def addAll(docIds: List[Long]): Unit = docIds.foreach(add)

  override def remove(docId: Long): Unit = _map.remove(docId)

  override def removeAll(docIds: List[Long]): Unit = docIds.foreach(remove)

  override def postings: mutable.Map[Long, Long] = _map

  def tfidfs(idf: Double): mutable.Map[Long, Double] = _map.map(t => (t._1, t._2 * idf))
}
