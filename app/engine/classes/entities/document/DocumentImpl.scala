package engine.classes.entities.document

import engine.traits.entities.Document

/**
  * Created by koord on 30.11.2016.
  */
class DocumentImpl(var _text:String, var _id: Long) extends Document {

  override def text: String = _text

  override def text_= (value:String): Unit = _text = value

  override def id: Long = _id

  override def id_= (value:Long): Unit = _id = value
}
