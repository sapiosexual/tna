package engine.classes.entities.cosine

import engine.classes.structures.indeces.AbstractIndex
import engine.classes.utils.Utils
import engine.traits.entities.{Document, PostingList}

import scala.collection.JavaConversions._
import scala.collection.mutable

/**
  * Created by koord on 30.11.2016.
  */
class CosineComputer(index: AbstractIndex[PostingList]) {

  val norms: mutable.Map[Long, Double] = mutable.Map()

  def prepareNorms(docs: List[Document]): Unit = {
    docs.foreach(d => norms.put(d.id,
      norm(tfidf(d).values.toList))
    )
  }

  def norm(features: List[Double]): Double = Math.sqrt(features.map(d => d * d).sum)

  def tfidf(doc: Document): Map[String, Double] = Utils.tokenize(doc).groupBy(s => s)
    .map((t : (String, Iterable[String])) =>  (t._1, t._2.size * index.idf(t._1)))

  def processQuery(query: Document): Array[(Long, Double)] = {
    val queryTfidf = tfidf(query)
    val queryNorm = norm(queryTfidf.values.toList)

    queryTfidf.keys.flatMap(t => index.tfidfs(t).map(f => (f._1, f._2 * queryTfidf(t)))).groupBy(t => t._1)
      .map(t => (t._1, t._2.map(d => d._2).sum)).map(t => (t._1, t._2/(queryNorm * norms(t._1))))
      .toArray.sortBy(t => -t._2)
  }
}
