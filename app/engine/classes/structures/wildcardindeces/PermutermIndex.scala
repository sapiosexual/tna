package engine.classes.structures.wildcardindeces

import engine.traits.structures.WildcardIndex

/**
  * Created by koord on 19.11.2016.
  */
class PermutermIndex extends WildcardIndex {
  override def add(term: String): Unit = ???

  override def addAll(terms: String): Unit = ???

  override def remove(term: String): Unit = ???

  override def removeAll(term: String): Unit = ???

  override def lookup(term: String): List[String] = ???
}
