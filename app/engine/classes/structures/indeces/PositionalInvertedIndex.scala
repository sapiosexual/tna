package engine.classes.structures.indeces

import engine.classes.entities.postings.PositionalPostingList
import engine.traits.entities.Document
import engine.traits.structures.Index

import scala.collection.mutable

/**
  * Created by koord on 19.11.2016.
  */
class PositionalInvertedIndex extends Index[PositionalPostingList] {
  override def add(doc: Document): Unit = ???

  override def addAll(docs: List[Document]): Unit = ???

  override def remove(doc: Document): Unit = ???

  override def removeAll(ids: List[Document]): Unit = ???

  override def find(term: String): PositionalPostingList = ???

  override def idf(term: String): Double = ???

  override def tfidfs(term: String): mutable.Map[Long, Double] = ???

  override def intersect[L >: PositionalPostingList](postings1: L, postings2: L): PositionalPostingList = ???

  override def union[L >: PositionalPostingList](postings1: L, postings2: L): PositionalPostingList = ???

  override def negation[L >: PositionalPostingList](postings: L): PositionalPostingList = ???
}
