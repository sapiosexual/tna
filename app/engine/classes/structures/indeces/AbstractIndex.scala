package engine.classes.structures.indeces

import engine.traits.entities.PostingList
import engine.traits.structures.Index

/**
  * Created by koord on 30.11.2016.
  */
abstract class AbstractIndex[+POSTING_LIST <: PostingList] extends Index[POSTING_LIST] {

}
