package engine.classes.structures.indeces

import engine.classes.entities.postings.PostingListImpl
import engine.classes.utils.Utils
import engine.traits.entities.Document
import scala.collection.JavaConversions._

/**
  * Created by koord on 19.11.2016.
  */
class InvertedIndex extends AbstractIndex[PostingListImpl] {

  private var _index = Map[String, PostingListImpl]()
  private var _count = 0

  override def add(doc: Document): Unit = {
    _count = _count + 1
    Utils.tokenize(doc).foreach(s => if (_index.contains(s)) _index(s).add(doc.id) else _index = _index + (s -> new PostingListImpl()))
  }

  override def addAll(docs: List[Document]): Unit = docs.foreach(add)

  override def remove(doc: Document): Unit = {
    var flag = false
    Utils.tokenize(doc).foreach(s => {
      if (_index.contains(s)) {
        _index(s).remove(_count)
        flag = true
      }
    })
    if (flag) _count = _count - 1
  }

  override def removeAll(docs: List[Document]): Unit = docs.foreach(remove)

  override def idf(term: String) : Double = if(find(term).postings.nonEmpty) Math.log(_count/find(term).postings.size) else 0
  override def tfidfs(term: String) : scala.collection.mutable.Map[Long, Double] = find(term).tfidfs(idf(term))

  override def find(term: String): PostingListImpl = _index.getOrElse(term, new PostingListImpl)

  def printTokenize(doc: Document) = {
    println(Utils.tokenize(doc))
  }

  override def intersect[L >: PostingListImpl](postings1: L, postings2: L): PostingListImpl = ???

  override def union[L >: PostingListImpl](postings1: L, postings2: L): PostingListImpl = ???

  override def negation[L >: PostingListImpl](postings: L): PostingListImpl = ???
}
