package engine.classes.structures.indeces

import engine.classes.entities.postings.PostingListImpl
import engine.traits.entities.Document
import engine.traits.structures.Index

import scala.collection.mutable

/**
  * Created by koord on 19.11.2016.
  */
class BiwordIndex extends Index[PostingListImpl] {
  override def add(doc: Document): Unit = ???

  override def addAll(docs: List[Document]): Unit = ???

  override def remove(doc: Document): Unit = ???

  override def removeAll(ids: List[Document]): Unit = ???

  override def find(term: String): PostingListImpl = ???

  override def idf(term: String): Double = ???

  override def tfidfs(term: String): mutable.Map[Long, Double] = ???

  override def intersect[L >: PostingListImpl](postings1: L, postings2: L): PostingListImpl = ???

  override def union[L >: PostingListImpl](postings1: L, postings2: L): PostingListImpl = ???

  override def negation[L >: PostingListImpl](postings: L): PostingListImpl = ???
}
