package engine.classes.structures.indeces

import engine.classes.entities.postings.SkipPostingList
import engine.traits.entities.Document
import engine.traits.structures.Index

import scala.collection.mutable

/**
  * Created by koord on 19.11.2016.
  */
class SkipListInvertedIndex extends Index[SkipPostingList] {
  override def add(doc: Document): Unit = ???

  override def addAll(docs: List[Document]): Unit = ???

  override def remove(doc: Document): Unit = ???

  override def removeAll(ids: List[Document]): Unit = ???

  override def find(term: String): SkipPostingList = ???

  override def idf(term: String): Double = ???

  override def tfidfs(term: String): mutable.Map[Long, Double] = ???

  override def intersect[L >: SkipPostingList](postings1: L, postings2: L): SkipPostingList = ???

  override def union[L >: SkipPostingList](postings1: L, postings2: L): SkipPostingList = ???

  override def negation[L >: SkipPostingList](postings: L): SkipPostingList = ???
}
