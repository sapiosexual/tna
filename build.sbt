name := "tna"

version := "1.0"

lazy val `tna` = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.7"

libraryDependencies ++= Seq( jdbc , cache , ws   , specs2 % Test,  "org.apache.lucene" % "lucene-analyzers-common" % "4.6.0")

unmanagedResourceDirectories in Test <+= baseDirectory(_ / "target/web/public/test")

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"

libraryDependencies += "com.github.marklister" %% "product-collections" % "1.4.5"
